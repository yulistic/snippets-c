#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>

//#define FILE_PATH "/mnt/nfs_cli/file_test"
//#define FILE_PATH "./test.txt"
#define USAGE "Usage: filetest <open_r|open_w|open_rw|read|write|close>"

//static const char *test_file_path = "/mnt/nfs_cli/file_test";
typedef enum {FILE_OPEN, FILE_READ, FILE_WRITE, FILE_CLOSE, NO_OP} test_t;
typedef enum {R, W, RW} openmode_t;
static test_t test;
static openmode_t openmode;
static int fd;
char buf[(4 << 20)];
char file_path[100];

void
parse_input (char input[])
{
    if (strcmp(input, "open_r") == 0){
	// printf ("Arguments: %s\n", input);
	test = FILE_OPEN;
	openmode = R;
	printf ("Enter file name:");
	scanf ("%s", file_path);  // get file name.
    } else if (strcmp(input, "open_w") == 0){
	// printf ("Arguments: %s\n", input);
	test = FILE_OPEN;
	openmode = W;
	printf ("Enter file name:");
	scanf ("%s", file_path);  // get file name.
    } else if (strcmp(input, "open_rw") == 0){
	// printf ("Arguments: %s\n", input);
	test = FILE_OPEN;
	openmode = RW;
	printf ("Enter file name:");
	scanf ("%s", file_path);  // get file name.
    } else if (strcmp(input, "read") == 0){
	// printf ("Arguments: %s\n", input);
	test = FILE_READ;
    } else if (strcmp(input, "write") == 0){
	// printf ("Arguments: %s\n", input);
	test = FILE_WRITE;
    } else if (strcmp(input, "close") == 0){
	// printf ("Arguments: %s\n", input);
	test = FILE_CLOSE;
    } else if (strcmp(input, "exit") == 0){
	// printf ("Arguments: %s\n", input);
	printf ("Bye.\n");
	exit(0);
    } else {
	test = NO_OP;
	printf (USAGE);
    }
}

void
open_file(void)
{
    int flag;
    switch(openmode){
	case R:
	    //fd = open(file_path, O_RDONLY, S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH);
	    fd = open(file_path, O_RDONLY);
	    printf ("Open file(R). fd:%d path:%s\n", fd, file_path);
	    break;
	case W:
	    fd = open(file_path, O_CREAT|O_WRONLY, S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH);
	    printf ("Open file(W). fd:%d path:%s\n", fd, file_path);
	    break;
	case RW:
	    fd = open(file_path, O_CREAT|O_RDWR, S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH);
	    printf ("Open file(RW). fd:%d path:%s\n", fd, file_path);
	    break;
	default:
	    printf("Unsupported open mode.\n");
	    exit(1);
    }

    if (fd < 0) {
	printf("Error: File open failed(%s).\n", strerror(errno));
	exit(1);
    }

    printf ("File opened.");
}

void
do_read(void)
{
    int ret;
    ret = read(fd, buf, 5);

    if (ret < 0) {
	printf("Error: File read failed(%s).\n", strerror(errno));
    } else {
	printf ("%d bytes are read: %s", ret, buf);
    }
}

void
do_write(void)
{
    int ret;
    char str[100];

    printf ("What to write? ");
    scanf(" %[^\t\n]s", str);
    ret = write(fd, str, strlen(str));

    if (ret < 0) {
	printf("Error: File write failed(%s).\n", strerror(errno));
    } else {
	fsync(fd);
	printf ("%d bytes are written.", ret);
    }
}

void
close_file(void)
{
    if (close(fd) < 0) {
	printf("Error: File close failed(%s).\n", strerror(errno));
	exit(1);
    } else {
	fd = -1;
	printf ("Closed. fd:%d", fd);
    }
}

void do_test(void)
{
    switch (test)
    {
	case FILE_OPEN:
	    open_file();
	    break;
	case FILE_READ:
	    do_read();
	    break;
	case FILE_WRITE:
	    do_write();
	    break;
	case FILE_CLOSE:
	    close_file();
	    break;
	case NO_OP:
	    break;
	default:
	    printf("Unsupported test.\n");
	    exit(1);
	    break;
    }
}

int
main (int argc, char *argv[])
{
    char str[100];

/*    if (argc != 2) {
	printf (USAGE);
	return 1;
    } else {
	get_arg(argv);
    }
    */

    printf ("Starting file test.");
    while(1) {
	printf ("\n> ");
	scanf ("%s", str);
	// printf ("Entered value: %s\n", str);
	parse_input(str);
	do_test();
    }
    return 0;
}
